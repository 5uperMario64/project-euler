// https://projecteuler.net/problem=1
package projecteuler;

import java.util.ArrayList;
import java.util.Comparator;

public class Problem1 {
	
	private static final int sumOfAllMultiplesOf3or5below = 999;

	public static void main(String[] args) {
		solveWith(sumOfAllMultiplesOf3or5below);
	}
	
	private static void solveWith(int input) {
		ArrayList<Integer> al = genList(input);
		System.out.println(sumOfArrayListIntegers(al));
	}

	private static ArrayList<Integer> genList(int input) {
		ArrayList<Integer> output = new ArrayList<Integer>();
		short i = 1, j = 1;
		
		// here we detect all multiples of 3 which don't collide with the factors of 5
		int upperBound1 = Math.floorDiv(input, 3);
		while (i < upperBound1) {
			if(i%5 != 0) output.add(i*3);
			i++;
		}
		
		// here we detect all multiples of 5
		int upperBound2 = Math.floorDiv(input, 5);
		while(j < upperBound2) {
			output.add(j*5);
			j++;
		}
		return output;
	}
	
	private static String sumOfArrayListIntegers(ArrayList<Integer> al) {
		int result = 0;
		for (int val : al)
			result += val;
		return Integer.toString(result) ;
	}
}

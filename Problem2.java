// https://projecteuler.net/problem=2
package projecteuler;

public class Problem2 {
	
	private static final int upperBound = 4000000;

	public static void main(String[] args) {
		solveWith(upperBound);
	}

	private static void solveWith(int input) {
		int sum = 0;
		
		int firstLastFib = 2;
		int secondLastFib = 1;
		int newVal = 0;
		
		while(newVal <= input) {
			newVal = firstLastFib + secondLastFib;
			firstLastFib = swapMe(newVal, newVal = firstLastFib);
			secondLastFib = swapMe(newVal, newVal = secondLastFib);
			
			if(newVal%2 == 0) sum += newVal;
		}
		System.out.println(sum);
	}
	
	public static int swapMe(int itself, int dummy) {
	    return itself;
	}
}
